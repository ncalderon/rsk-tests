const preCompiled = require("@rsksmart/rsk-precompiled-abis");
const Web3 = require("web3");

const rskHostUrl = "http://localhost:4450";
const bridgeClient = preCompiled.bridge.build(new Web3(rskHostUrl));

// Returns the BTC federation address
bridgeClient.methods
  .getFederationAddress()
  .call()
  .then(address => console.log(`Federation address: ${address}`));

bridgeClient.methods
  .getRetiringFederationAddress()
  .call()
  .then(address => console.log(`Retiring Federation address: ${address}`));

bridgeClient.methods.getBtcBlockchainBestChainHeight()
  .call()
  .then(result => console.log(`BTC blockchain best chain height: ${result}`));

bridgeClient.methods.getStateForBtcReleaseClient()
  .call()
  .then(result => console.log(`State for BTC release client: ${result}`));

bridgeClient.methods.getBtcBlockchainInitialBlockHeight()
  .call()
  .then(result => console.log(` Initial block height: ${result}`));

bridgeClient.methods.getBtcBlockchainBlockHashAtDepth(2)
  .call()
  .then(result => console.log(` BTC blockchain blockhash at depth ${2}: ${result}`));

bridgeClient.methods.getFederationSize() // 13
  .call()
  .then(result => console.log(` Federation size: ${result}`));

bridgeClient.methods.getRetiringFederationSize() // 13
  .call()
  .then(result => console.log(`Retiring Federation size: ${result}`));

bridgeClient.methods.getFederationCreationTime()
  .call()
  .then(result => console.log(`Federation creation time: ${result}`));

// bridgeClient.methods.getFederatorPublicKey(1)
//     .call()
//     .then(result => console.log(`Federator public key: ${result}`));