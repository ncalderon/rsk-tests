const fs = require('fs');
const Web3 = require('web3');
const preCompiled = require("@rsksmart/rsk-precompiled-abis");
const web3 = new Web3('https://public-node.rsk.co');

const bridgeClient = preCompiled.bridge.build(web3);
var wait = (msec) => new Promise((resolve) => setTimeout(resolve, msec));

(async() => {
  const latestBlockNumber = await web3.eth.getBlockNumber();
  const startBlockNumber = 6095189 - 9000;
  const endBlockNumber = latestBlockNumber;

  const stream = fs.createWriteStream('dataset.csv');

  stream.write('Block Number,Timestamp\n');

  for (let blockNumber = startBlockNumber; blockNumber <= endBlockNumber; blockNumber++) {
    const block = await web3.eth.getBlock(blockNumber);

    const line = `${block.number},${block.timestamp}\n`;

    stream.write(line);
    await wait(500);
    // setTimeout(function(){
    //   getBlock(i)
    // }, 1000);
  }

  stream.end();
})();

const getBlock = async (stream, blockNumber) => {
  const block = await web3.eth.getBlock(blockNumber);

  const line = `${block.number},${block.timestamp}\n`;

  stream.write(line);
}