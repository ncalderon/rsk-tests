const mempoolJS = require("@mempool/mempool.js");
const fs = require('fs');

var wait = (msec) => new Promise((resolve) => setTimeout(resolve, msec));

(async () => {
  const { bitcoin: { blocks } } = mempoolJS({
    hostname: 'mempool.space',
    network: 'testnet' // 'testnet' | 'mainnet'
  });

  const param_start_height = undefined;
  const start_height = param_start_height === undefined? await blocks.getBlocksTipHeight(): param_start_height;

  const headers = "height,timestamp";
  const keys = headers.split(",");

  let rows = [];
  let limit = 10000;
  let next_start_height = start_height;
  while(rows.length < limit){
    await wait(300);
    const lastBlocks = await blocks.getBlocks({ start_height: next_start_height });
    const size = lastBlocks.length;
    next_start_height -= size;

    for (const block of lastBlocks) {
      let row = "";
      for (const key of keys) {
        row += block[key] + ",";
      }
      row = row.substring(0, row.length-1);
      row += "\n";
      rows.push(row);
    }
  }


  let filename = start_height + "-" + limit;
  let file = fs.createWriteStream(filename + '.csv');
  file.on('error', function(err) { /* error handling */ });
  rows.forEach(row => {
    file.write(row);
  })
  file.end();
})();