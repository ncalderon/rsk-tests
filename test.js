const Web3 = require('web3');
const preCompiled = require('@rsksmart/rsk-precompiled-abis');

const url = "http://localhost:7777";
//const url = "http://15.237.118.190:4444";
//const url = "https://public-node.rsk.co";
//const url = "https://public-node.testnet.rsk.co";

//const url = "http://13.38.77.151:7777";

let web3 = new Web3(url)
web3.eth.getBlock('latest').then(console.log)

const bridgeInstance = preCompiled.bridge.build(web3);

//web3Client.eth.getBlock("latest").then(console.log);
// web3Client.eth.getPendingTransactions().then(
//     value => {
//         console.log(value)
//     }, reason => {
//         console.log(reason)
//     }
// )

const run = async () => {
  const result = await bridgeInstance.methods.getFederationAddress().call();
  console.log(result);
  const fedSize = await bridgeInstance.methods.getFederationSize().call();

  const fedPubKeys = [];
  for (let i = 0; i < fedSize; i++) {
    let fedPubKey = await bridgeInstance.methods.getFederatorPublicKeyOfType(i, 'rsk').call();
    fedPubKeys.push(fedPubKey.substring(2));
  }

  const addresses = [];
  for (const fedPubKeysKey in fedPubKeys) {
    const address = Buffer.from((fedPubKeysKey, 'hex').slice(-20).toString('hex'))
    addresses.push({[fedPubKeys]: address})
  }
  console.log(fedPubKeys);
  console.log(addresses);
}

run();

/*

// BTC
[
  '020dfaa261fb7ea99f29b2f25cec9c49b6fe47daca5138b4f5443ba261547817f6',
  '0303d2e83183329a7fd3020b93d5d58e9ec74ecc6f49cf84b1886045b6436729ad',
  '03be2c679f737910823678cbb70dcec0e31dfa05e4b8cb1bff7195fce96a7e1523'
]

// RSK
[
  '020dfaa261fb7ea99f29b2f25cec9c49b6fe47daca5138b4f5443ba261547817f6',
  '0303d2e83183329a7fd3020b93d5d58e9ec74ecc6f49cf84b1886045b6436729ad',
  '039c477739f727e3c278d60c37e4452a92951cc9927fe3b82c9e56a465204d9728'
]

*/
