const preCompiled = require("@rsksmart/rsk-precompiled-abis");
const Web3 = require("web3");

const rskHostUrl = "https://public-node.rsk.co";
const bridgeClient = preCompiled.bridge.build(new Web3(rskHostUrl));


//bridgeClient.methods.getFederatorPublicKey(1).call().then(result => console.log(`Federator public key: ${result}`));

(async () => {

  const address = await bridgeClient.methods.getFederationAddress().call();
  console.log(`Federation address: ${address}`);


  const btcBlockBestChainInitialBlockHeight = await bridgeClient.methods.getBtcBlockchainBestChainHeight().call();
  console.log(`btcBlockBestChainInitialBlockHeight: ${btcBlockBestChainInitialBlockHeight}`);

  const federator_pks = [];
  const federation_size = await bridgeClient.methods.getFederationSize().call();
  console.log(` Federation size: ${federation_size}`);
  for (let i = 0; i < federation_size; i++) {
    const fedBtcPk = await bridgeClient.methods.getFederatorPublicKeyOfType(i, "btc").call();
    const fedRskPk = await bridgeClient.methods.getFederatorPublicKeyOfType(i, "rsk").call();
    const fedMstPk = await bridgeClient.methods.getFederatorPublicKeyOfType(i, "mst").call();
    federator_pks.push({
      "btc": fedBtcPk,
      "rsk": fedRskPk,
      "mst": fedMstPk
    });
  }

  console.log(`Federator pks: ${JSON.stringify(federator_pks, null, 2)}`);
})();