let Web3 = require('web3');
let client = new Web3('http://127.0.0.1:7777');

function calculateAccumulatedDifficultyBetweenTheseBlocks(newestBlock, oldestBlock) {
    return newestBlock.totalDifficulty - oldestBlock.totalDifficulty;
}

function displayAccumulatedDifficultyCalculation(olderBlock, lastBlock) {
    let blocksClount = lastBlock.number - olderBlock.number;
    let accumulatedDifficulty = calculateAccumulatedDifficultyBetweenTheseBlocks(lastBlock, olderBlock);
    console.log(`- Accumulated difficulty for past ${blocksClount} blocks:`);
    console.log(`newest block: ${lastBlock.number}, hash ${lastBlock.hash}`);
    console.log(`older block: ${olderBlock.number}, hash ${olderBlock.hash}`);
    console.log(`
        ${lastBlock.totalDifficulty} -
        ${olderBlock.totalDifficulty}
        ----------------------------
        ${accumulatedDifficulty}
        (0x${accumulatedDifficulty.toString(16)})
    `);
}

const displayAverageDifficulty = async(startingBlock, numberOfBlocks) => {
    let totalDifficulty = BigInt(0);
    for (let i = startingBlock; i<startingBlock+numberOfBlocks; i++) {
        let block = await client.eth.getBlock(i, true);
        totalDifficulty += BigInt(block.difficulty);
    }

    let averageDifficulty = BigInt(totalDifficulty) / BigInt(numberOfBlocks);
    console.log(`The average difficulty between blocks ${startingBlock} and ${startingBlock + numberOfBlocks} is ${averageDifficulty}`);
}

(async() => {
    let lastBlock = await client.eth.getBlock('latest', true);
    let olderBlock = await client.eth.getBlock(lastBlock.number - 2, true);
    
    displayAccumulatedDifficultyCalculation(olderBlock, lastBlock);    
    // await displayAverageDifficulty(3489000, 190);
})();
