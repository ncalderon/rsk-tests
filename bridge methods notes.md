getFederationAddress
: Returns the BTC federation address.

getBtcBlockchainBestChainHeight
: Returns the height of the main chain (the chain with more PoW).

getStateForBtcReleaseClient
: Returns the pending btc transactions to be released that needs to be signed.

getStateForDebugging
: Returns the state of the bridge, in a way that could be used for debugging

getBtcBlockchainInitialBlockHeight
: Returns the height of either the genesis block or a checkpoint block.

getBtcBlockchainBlockHashAtDepth
: Returns the blockhash that is found at the specified depth. For a depth 0 the blockhash returned will belong to the best block.

getBtcTxHashProcessedHeight
: For a BTC transaction hash given as an input, this method will return in which block was processed in the RSK blockchain

getFederationSize
: Returns the number of members of the federation.

getFederationThreshold
: Returns the federation's minimun required signatures to process a release btc transaction.

getFederationCreationTime
: Returns the creation time of the current active federation

getRetiringFederationCreationTime
: Returns the creation time of the retiring federation

getFederationCreationBlockNumber
: Returns the block number since the current active federation was created. // TODO: REVIEW blocknumber and blockheight refer both to the same thing, the depth of the block in the blockchain..

getRetiringFederationCreationBlockNumber
: Returns the block number since the retiring federation was created, or null if none retiring federation exists.// TODO: REVIEW

getFederatorPublicKey
: Returns the public key of a specific federator. NOTE: This is deprecated.

getRetiringFederatorPublicKeyOfType
: Returns the federator public key address of the specified type (rsk, btc, mst)


getRetiringFederationAddress
: Returns the address of the previous federation that is now in a retiring state and has been replaced for the new active one. 

getRetiringFederationSize
: Returns the total number of public keys of the retiring federation

getRetiringFederationThreshold
: Returns the retiring federation's minimun required signatures to process a release btc transaction.

getPendingFederationHash
: Returns the currently pending federation hash, or null if none exists. Allows check if the pending federation is correct.

getPendingFederationSize
: Returns the currently pending federation size, or null if none exist. Pending Federation refers to a new federation that is pending to be activated to replace the current active federation.

getFeePerKb
: Returns current fee per kb registed in the storage. The fee per kb is the amount that is charged per transaction. This amount is calculated from the satoshi per byte(the amount of satoshi a user is willing to pay to have their transaction processed) multiply by the transaction size in bytes.

getLockingCap
: Returns the capacity at which the bridge can lock rbtc?

getActiveFederationCreationBlockHeight
: Returns the block height since the current active federation was created. // TODO: REVIEW

getLockWhitelistSize
: Returns the size of the addresses put in the white list?

getLockWhitelistAddress
: Returns the specified address put in the white list?

getLockWhitelistEntryByAddress
: Returns the white list record of the specified address from the storage?
