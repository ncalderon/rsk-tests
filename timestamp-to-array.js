const fs = require('fs');
const readline = require('readline');
const events = require('events');

(async () => {

  const filenames = [
  "706000-3000.csv",
  "707000-1000.csv",
  "709000-1000.csv",
  "709565-2000.csv",
  "800000-1000.csv",
  "800565-500.csv",
  "805500-50.csv",
  "808565-100.csv",
  "810564-1000.csv",
  "810565-20.csv"
  ];

  const allData = [];
  for (const filename of filenames) {
    const fileStream = fs.createReadStream(filename);
    const rl = readline.createInterface({
      input: fileStream,
      crlfDelay: Infinity
    });

    let data = [];
    rl.on('line', (line) => {
      if (line.indexOf("timestamp") > -1){
        return;
      }
      let values = line.split(",");
      data.push(values[1]);
    });

    rl.on('close', () => {
      allData.push(data);
      console.log(data)
      console.log('Finished reading the file: ' + filename);
    });

    await events.once(rl, 'close');
  }



  let filename = "timestamps-btc-blocks-multi-array";
  let file = fs.createWriteStream(filename + '.json');
  file.on('error', function(err) { /* error handling */ });
  file.write("[" +  "\n");
  allData.forEach(row => {
    file.write(JSON.stringify(row) + ",\n");
  })
  file.write("]" +  "\n");
  file.end();
})();