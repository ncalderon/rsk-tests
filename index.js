const Web3 = require('web3');
const preCompiled = require('@rsksmart/rsk-precompiled-abis');

const testnetUrl = "https://public-node.testnet.rsk.co";

const testnetBridge = preCompiled.bridge.build(new Web3(testnetUrl));

testnetBridge.methods
    .getFederationAddress()
    .call()
    .then(address => console.log(`Federation testnet address: ${address}`));

testnetBridge.methods
  .getActivePowpegRedeemScript()
  .call()
  .then(result => console.log(`Federation redeemscript testnet: ${result}`));

testnetBridge.methods
  .getNextPegoutCreationBlockNumber()
  .call()
  .then(result => console.log(`getNextPegoutActivationHeight testnet: ${result}`));

/*

// getBtcBlockchainBestChainHeight : returns the height of the main chain (the chain with more PoW)
testnetBridge.methods.getBtcBlockchainBestChainHeight()
    .call()
    .then(result => console.log(`BTC blockchain best chain height: ${result}`));


// Returns the pending btc transactions to be released that needs to be signed or broadcast by the federators.
// testnetBridge.methods.getStateForBtcReleaseClient()
//     .call()
//     .then(result => console.log(`State for BTC release client: ${result}`));


// testnetBridge.methods.getStateForDebugging()
//     .call()
//     .then(result => console.log(`State for debugging: ${result}`));

// Returns the height of either the Genesis block or a checkpoint block.
testnetBridge.methods.getBtcBlockchainInitialBlockHeight()
    .call()
    .then(result => console.log(`Initial block height: ${result}`));

// Returns the blockhash that is found at the specified depth. For a depth 0 the blockhash returned will belong to the last block.
testnetBridge.methods.getBtcBlockchainBlockHashAtDepth(2)
    .call()
    .then(result => console.log(`BTC blockchain blockhash at depth ${2}: ${result}`));

// For a BTC transaction hash given as an input, this method will return its height in the RSK blockchain
testnetBridge.methods.getBtcTxHashProcessedHeight("927a6132c865c73e868eea47c32561bf3b4206e556a1b4c4cb6e638701f8dda1")
    .call()
    .then(result => console.log(`BTC tx hash processed height: ${result}`));

// Returns the number of publickeys of the federation
testnetBridge.methods.getFederationSize() // 5
    .call()
    .then(result => console.log(`Federation size: ${result}`));


 // Returns the federation's minimun required signatures to process a release btc transaction.   
testnetBridge.methods.getFederationThreshold()
    .call()
    .then(result => console.log(`Federation threshold: ${result}`));

// Returns the creation time of the current active federation
testnetBridge.methods.getFederationCreationTime()
    .call()
    .then(result => console.log(`Federation creation time: ${result}`));

// TODO: Investigate why always got an error executing this method.
// Assuming that this input index refers to one of the public keys of the federation I tried different indexes from 0 - 10,
// but I kept getting an error

// Returns the block number from where the current active federation was created
testnetBridge.methods
    .getFederationCreationBlockNumber()
    .call()
    .then(result => console.log(`fedCreationTimeBlockNumber: ${result}`));

// Returns the address of the previous federation that is now in a retiring state and has been replaced for the new active one.
testnetBridge.methods
    .getRetiringFederationAddress()
    .call()
    .then(result => console.log(`retiringFederationAddress: ${result}`));

// Returns the total number of public keys of the retiring federation
testnetBridge.methods
    .getRetiringFederationSize()
    .call()
    .then(result => console.log(`getRetiringFederationSize: ${result}`));

//  Returns the retiring federation's minimun required signatures to process a release btc transaction.
testnetBridge.methods
    .getRetiringFederationThreshold()
    .call()
    .then(result => console.log(`getRetiringFederationThreshold: ${result}`));


// TODO: It's always returning "null". Pending.
testnetBridge.methods
    .getRetiringFederatorPublicKeyOfType(1, "btc")
    .call()
    .then(result => console.log(`getRetiringFederatorPublicKeyOfType: ${result}`));

// Returns the currently pending federation hash, or null if none exists. TODO: Investigate further the purpose of this method
testnetBridge.methods
    .getPendingFederationHash()
    .call()
    .then(result => console.log(`getPendingFederationHash: ${result}`));

// Returns the currently pending federation size, or null if none exist. Pending Federation refers to a new federation that is pending to be activated to replace the current active federation.
testnetBridge.methods
    .getPendingFederationSize()
    .call()
    .then(result => console.log(`getPendingFederationSize: ${result}`));

testnetBridge.methods
    .getPendingFederationSize()
    .call()
    .then(result => console.log(`getPendingFederationSize: ${result}`));

/!**
 * Returns current fee per kb in BTC. TODO: Investigate further the different uses of this method
 *!/
testnetBridge.methods
    .getFeePerKb()
    .call()
    .then(result => console.log(`getFeePerKb: ${result}`));


// TODO: PENDING
// testnetBridge.methods
//     .getBtcTransactionConfirmations() // too many inputs
//     .call()
//     .then(result => console.log(`getBtcTransactionConfirmations: ${result}`));

testnetBridge.methods
    .getLockingCap()
    .call()
    .then(result => console.log(`getLockingCap: ${result}`));

testnetBridge.methods
    .getActiveFederationCreationBlockHeight()
    .call()
    .then(result => console.log(`getActiveFederationCreationBlockHeight: ${result}`));

// testnetBridge.methods
//     .getPendingFederatorPublicKey(0) // fails
//     .call()
//     .then(result => console.log(`getPendingFederatorPublicKey: ${result}`));

testnetBridge.methods
    .getPendingFederatorPublicKeyOfType(0, "rsk") // null
    .call()
    .then(result => console.log(`getPendingFederatorPublicKeyOfType: ${result}`));

testnetBridge.methods
    .getLockWhitelistSize() // 12
    .call()
    .then(result => console.log(`getLockWhitelistSize: ${result}`));

testnetBridge.methods
    .getLockWhitelistAddress(0) // mim9yonpNo7Bpau5GJRueJ5zYGPRNVKp4C
    .call()
    .then(result => console.log(`getLockWhitelistAddress: ${result}`));

testnetBridge.methods
    .getLockWhitelistEntryByAddress("mim9yonpNo7Bpau5GJRueJ5zYGPRNVKp4C") // 0
    .call()
    .then(result => console.log(`getLockWhitelistEntryByAddress: ${result}`));
    
// testnetBridge.methods
//     .addLockWhitelistAddress("mim9yonpNo7Bpau5GJRueJ5zYGPRNVKp4C", 4) // fails
//     .call()
//     .then(result => console.log(`addLockWhitelistAddress: ${result}`));

// testnetBridge.methods
//     .createFederation() // -10
//     .call()
//     .then(result => console.log(`createFederation: ${result}`));

testnetBridge.methods
    .getRetiringFederationCreationTime()
    .call()
    .then(result => console.log(`getRetiringFederationCreationTime: ${result}`));

testnetBridge.methods
    .getRetiringFederationCreationBlockNumber()
    .call()
    .then(result => console.log(`getRetiringFederationCreationBlockNumber: ${result}`));

// testnetBridge.methods
//     .getRetiringFederatorPublicKey(0) // fails
//     .call()
//     .then(result => console.log(`getRetiringFederatorPublicKey: ${result}`));

testnetBridge.methods
    .getFederatorPublicKeyOfType(1, "rsk")
    .call()
    .then(result => console.log(`getFederatorPublicKeyOfType: ${result}`));

testnetBridge.methods
    .getBtcBlockchainBestBlockHeader() // 0x00008020c00bbd6205a7dcb5a48a2a3606bfe4751c7c83374353dbed25000000000000003cd503854930454b600d70cbc189a7593e6d16c4cf4521ddc40ec13f1a92ec36515c9261e701331933deadc2
    .call()
    .then(result => console.log(`getBtcBlockchainBestBlockHeader: ${result}`));

// testnetBridge.methods
//     .getBtcBlockchainBlockHeaderByHash("00000000000000000001baa214c5ced6be274885861a1c6744d788d34db6ad46") // fails
//     .call()
//     .then(result => console.log(`getBtcBlockchainBlockHeaderByHash: ${result}`));

testnetBridge.methods
    .getBtcBlockchainBlockHeaderByHeight(1) // null
    .call()
    .then(result => console.log(`getBtcBlockchainBlockHeaderByHeight: ${result}`));

testnetBridge.methods
    .getBtcBlockchainParentBlockHeaderByHash("0x00000000000000000005c4e2f7a26b9a3285d5478eee530bf3f65b762c4ba42c") // null
    .call()
    .then(result => console.log(`getBtcBlockchainParentBlockHeaderByHash: ${result}`));
*/
