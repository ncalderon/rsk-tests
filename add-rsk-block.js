const fs = require('fs');
const readline = require('readline');
const events = require('events');

(async () => {

  let rawRskBlocks = fs.readFileSync('results.json');
  let rskBlocks = JSON.parse(rawRskBlocks);

  const filenames = [
  "706000-3000.csv",
  "707000-1000.csv",
  "709000-1000.csv",
  "709565-2000.csv",
  "800000-1000.csv",
  "800565-500.csv",
  "805500-50.csv",
  "808565-100.csv",
  "810564-1000.csv",
  "810565-20.csv"
  ];


  for (const filename of filenames) {
    const fileStream = fs.createReadStream(filename);
    const rl = readline.createInterface({
      input: fileStream,
      crlfDelay: Infinity
    });

    let data = [];
    rl.on('line', (line) => {
      if (line.indexOf("timestamp") > -1){
        return;
      }
      let values = line.split(",");
      let timestamp = values[1];
      const rskBlock = rskBlocks[timestamp];
      data.push(rskBlock + "," + line);
    });

    rl.on('close', () => {
      console.log(data)
      console.log('Finished reading the file: ' + filename);
    });

    await events.once(rl, 'close');

    let datasetFilename = "datasets/" + filename;
    let file = fs.createWriteStream(datasetFilename);
    file.on('error', function(err) { /* error handling */ });
    file.write("rsk,btc,timestamp" +  "\n");
    data.forEach(row => {
      file.write(row + "\n");
    })
    file.end();
  }




})();
